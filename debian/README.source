Release process for libSavitar
------------------------------

It is highly recommended to use git-buildpackage (gbp) to prepare updated
packages. This tool will automatically verify the Debian tree against
the upstream sources and generate separate source and debian tarballs.

Packaging instructions (1 and 2 only need to be done once):

1.  Obtain the sources:

  $ gbp clone ssh://git.debian.org/git/3dprinter/packages/libsavitar.git
  $ cd libsavitar

2.  Import the latest upstream release:

  $ git fetch --all
  $ gbp import-orig --uscan

  This will fetch the latest upstream release into the upstream branch
  and create and commit a pristine-tar from it.

3.  Update the changelog:

  $ dch -v <UPSTREAM_VERSION>-<DEBIAN_VERSION>

  Replace UPSTREAM_VERSION with the version fetched in step 3 and
  DEBIAN_VERSION with 1. If you are creating a new Debian release
  from a previous package version, increment DEBIAN_VERSION by 1 each time.
  This will open a text editor where you can edit the changelog entry.
  Include a "Closes #..." entry if this release fixes a Debian bug.

4.  Refresh and verify the patches:

  $ quilt push -a

  This applies all patches. If you encounter any errors, fix them.
  Make sure to refresh all patches that have changed and remove those that
  aren't needed any more.
  Commit the updated patches using git.

5.  Build against the upstream tree:

  $ gbp buildpackage -us -uc

6.  Verify that the build succeeds, then update the symbols file if necessary:

  $ dpkg-gensymbols -plibsavitar0 -Idebian/libsavitar0.symbols -Pdebian/libsavitar0 -Olibsavitar.symbols.update -v<UPSTREAM_VERSION>

  (only if changes are displayed:)

  $ sed 's/ \(_.*\) \(.*\)/ (c++)"\1" \2/' libsavitar0.symbols.update | c++filt > libsavitar0.symbols.demangled
  $ mv libsavitar0.symbols.demangled debian/libsavitar0.symbols
  $ git add debian/libsavitar0.symbols
  $ git commit -m "Updated library symbols"

7.  Clean and build again:

  $ debian/rules clean; quilt pop -a; rm -rf .pc
  $ gbp buildpackage

  (this may prompt you for your GPG passphrase)

8.  Verify that you don't have any lintian warnings:

  $ linitian -I

9. Merge your changes back into the debian tree, tag and push everything:

  $ gbp buildpackage --git-tag --git-sign-tags
  $ git push origin master upstream pristine-tar
  $ git push --tags origin

10. Sign your package and release using dput or whichever method you prefer:

  $ debsign
  $ dput <RELEASE_REPOSITORY> ../libsavitar_<VERSION>.changes
